d3.json("data/data.json").then(data => console.log(data));

var margin = {
	left:80,
	right:20,
	top:50,
	bottom:100
};

var height = 500 - margin.top - margin.bottom,
	width = 800 - margin.left - margin.right;

var g = d3.select("#chart-area")
	.append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
	.append("g")
	.attr("transform", "translate(" + margin.left +
			", " + margin.top + ")"
	);

let time = 0;
const continentColors = d3.schemeCategory10;
const scaleColors = d3.scaleOrdinal(continentColors);

// Scales
var x = d3.scaleLog()
	.base(10)
	.range([0, width])
	.domain([142, 150000]);
console.log(x)
var y = d3.scaleLinear()
	.range([height, 0])
	.domain([0, 90]);
console.log(y)
var area = d3.scaleLinear()
	.range([25*Math.PI, 1500*Math.PI])
	.domain([2000, 1400000000]);
console.log(area);
var continentColor = d3.scaleOrdinal(d3.schemePastel1);

// Labels
var xLabel = g.append("text")
	.attr("y", height + 50)
	.attr("x", width / 2)
	.attr("font-size", "20px")
	.attr("text-anchor", "middle")
	.text("GDP Per Capita ($)");

var yLabel = g.append("text")
	.attr("transform", "rotate(-90)")
	.attr("y", -40)
	.attr("x", -170)
	.attr("font-size", "20px")
	.attr("text-anchor", "middle")
	.text("Life Expectancy (Years)");

// X Axis
var xAxisCall = d3.axisBottom(x)
	.tickValues([400, 4000, 40000])
	.tickFormat(d3.format("$"));

g.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + height +")")
	.call(xAxisCall);

// Y Axis
var yAxisCall = d3.axisLeft(y)
	.tickFormat(d => +d);
g.append("g")
	.attr("class", "y axis")
	.call(yAxisCall);

const config = {
	chart: {
			width: 600,
			height: 300,
	},
	margin:{
			left:80,
			right:20,
			top:50,
			bottom:100
	},
	intervalDuration: 100,
};

const var_12 = 12;
const yearLabel = g
	.append('text')
	.attr('x', config.chart.width)
	.attr('y', config.chart.height - var_12)
	.attr('font-size', '46px')
	.attr('font-weight', 'bold')
	.attr('text-anchor', 'middle')
	.attr('fill', 'black')
	.text('');

const continentSelect = d3.select('#chart-area');

let interval = null
d3.json("data/data.json").then(rawData => {
	console.log(rawData)

	const data = rawData.map(dataSet => {
			return {
					...dataSet,
					countries: dataSet.countries
							.filter(country => country.population && country.income && country.life_exp)
							.map(country => ({
									...country,
									income: +country.income,
									life_exp: +country.life_exp,
							}))
			}
	})
	const getNext = () => data[time++ % data.length]
	const step = () => update(getNext())
	interval = setInterval(step, config.intervalDuration)
});

function update(dataSet) {
	yearLabel.text(dataSet.year);
	const countries = dataSet.countries.filter(country => {
			return country.continent;
	});

	const circles = g.selectAll('circle')
									.data(countries, d => d.country );

	circles
			.enter()
			.append('circle')
			.attr('cx', d => x(d.income))
			.attr('cy', d => y(d.life_exp))
			.merge(circles)
			.transition()
			.duration(config.intervalDuration)
			.ease(d3.easeLinear)
			.attr('cx', d => x(d.income))
			.attr('cy', d => y(d.life_exp))
			.attr('r', d => Math.sqrt(area(d.population) / Math.PI))
			.attr('fill', d => continentColor(d.continent));

};